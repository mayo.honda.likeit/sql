CREATE TABLE item_category(
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name varchar(256) NOT NULL
);

CREATE TABLE item_category(
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name varchar(256) NOT NULL
);

ALTER TABLE item
    CHANGE COLUMN item_price total_price int NOT NULL;

ALTER TABLE item
    CHANGE COLUMN total_price item_price int NOT NULL;

SELECT b.category_name,SUM(a.item_price) AS total_price
FROM 
    item a 
INNER JOIN
    item_category b
ON
    a.category_id = b.category_id

GROUP BY
    b.category_name;
